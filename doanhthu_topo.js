const changeText = () => {
    var viewDetail = apex.region("anhvtdetail").widget().interactiveGrid("getViews").grid.view$;
    console.log('viewMaster', viewDetail);
}
const changeColor = () => {
	var viewDetail = apex.region("anhvtdetail").widget().interactiveGrid("getViews").grid.view$;
    console.log('viewMaster', viewDetail);
	var selected = viewDetail.grid("getSelectedRecords");
	console.log('aaaaa', selected);
}

const doanhthu_topo = () => {
    try {
        var notification = [];
        var error_type = [];
        var master_model = apex.region("master").widget().interactiveGrid("getViews", "grid").model;
        var detail_model = apex.region("detail").widget().interactiveGrid("getViews", "grid").model;
        //console.log('HIEU',detail_model);

        var ms_count_row = master_model.getTotalRecords();
        var tkco = detail_model.getFieldKey("CREDIT_ACCOUNT_ID");
        var flagDeleteDetail = detail_model.getFieldKey("FlagDelete");
        var check_duplicate = 0;
        var arr_tkco = [];
        var ms_count_row = master_model.getTotalRecords();
        var dt_count_row = detail_model.getTotalRecords();

        var ms_total_nguyente = 0;
        var ms_total_quydoi = 0;
        var ms_total_thuent = 0;
        var ms_total_thueqd = 0;
        var dt_total_nguyente = 0;
        var dt_total_quydoi = 0;
        var dt_total_thuent = 0;
        var dt_total_thueqd = 0;

        var save = 0;
        // var detail_model_test = apex.region("detail").widget().interactiveGrid("getViews", "grid").model;
        // list check xem giữ liệu có trùng nhau ko
        var listDupLoaiThueGTGT = [];
        var listDupDTThueGTGT = [];
        var listDupThueRaVao = [];
        var listDupLoaiThue = [];
        //var listDupLoaiHoaDon = [];
        var listDupLoaiDTNo = [];
        var listDupDTNo = [];
        var listDupLoaiDTCo = [];
        var listDupDTCo = [];
        var listDupNhomThue = [];
        var listDupLoaiDTDongThue = [];
        var listDupDTDongThue = [];
        var listDupKM1 = [];
        var listDupKM2 = [];
        var listDupKM3 = [];
        var messageListError = '';
        console.log('HIEU', detail_model);
        //kiểm tra số dòng tách (detail) phải lớn hơn dòng gốc (master)
        dt_xoa = detail_model.getFieldKey("IS_DELETED");

        var objectAmountUnique = detail_model._data.filter(x => x[flagDeleteDetail] == 0).reduce((obj, v) => {
            obj[v[64]] = (obj[v[64]] || 0) + 1;
            return obj;
        }, {});
        console.log('aaaaaaaaaaaaaaaaaa', objectAmountUnique);

        var messageErrorRowInvalid = '';
        Object.entries(objectAmountUnique).forEach(key => {
            console.log('key', key);
            console.log('value', objectAmountUnique[key])
            if (key[1] < 2) {
                messageErrorRowInvalid += `SỐ DÒNG TÁCH KHÔNG HỢP LÊ. BẮT BUỘC Id ${key[0]} tối thiếu là 2 dòng \n`;
            }
        })

        if (messageErrorRowInvalid != '') {
            apex.message.alert(messageErrorRowInvalid);
            return;
        }
        detail_model.forEach(function (r) {
            if (r[dt_xoa] != 'Y') {
                arr_tkco.unshift(r[tkco]);
            }
        });

        //kiểm tra tài khoản có ko dc để trống
        var checkCreditAccountIdNull = detail_model._data.filter(x => x[tkco] === "" && x[flagDeleteDetail] == 0);
        if (checkCreditAccountIdNull.length > 0) {
            apex.message.alert("`Tài khoản có` không được để trống");
            return;
        }
        // kiểm tra tài khoản có trùng
        detail_model.forEach(function (r) {
            if (r[dt_xoa] != 'Y' && r[flagDeleteDetail] == 0) {
                var count2 = 0;
                var position = -1;
                val = r[tkco];
                position = arr_tkco.indexOf(val);
                while (position != -1) {
                    count2 = count2 + 1;
                    position = arr_tkco.indexOf(val, position + 1);
                }
                if (count2 > 1) {
                    check_duplicate = 1;
                }
            }
        });
        if (check_duplicate == 1) {
            apex.message.alert('TRÙNG TÀI KHOẢN CÓ!');
            return;
        }

        // //kiểm tra tổng giá trị của 4 cột: nguyên tế, quy đổi, thuế nt, thuế qd của những dòng tách (detail) có bằng dòng gốc (master) không
        notification = '';
        for (var i = 0; i < master_model._data.length; i++) {
            doanhThuId = master_model.getFieldKey("ID");
            doanhThuDetailId = detail_model.getFieldKey("DOANH_THU_TOPO_ID");

            //lấy dữ liệu các cột trog bảng master	
            ms_nguyente = master_model.getFieldKey("O_AMOUNT");
            ms_quydoi = master_model.getFieldKey("C_AMOUNT");
            ms_thuent = master_model.getFieldKey("TAX_O_AMOUNT");
            ms_thueqd = master_model.getFieldKey("TAX_C_AMOUNT");

            //lấy dữ liệu các cột trog bảng detail
            dt_nguyente = detail_model.getFieldKey("O_AMOUNT");
            dt_quydoi = detail_model.getFieldKey("C_AMOUNT");
            dt_thuent = detail_model.getFieldKey("TAX_O_AMOUNT");
            dt_thueqd = detail_model.getFieldKey("TAX_C_AMOUNT");

            var valueDetail = detail_model._data.filter(x => x[doanhThuDetailId] == master_model._data[i][doanhThuId] && x[flagDeleteDetail] == 0);
            // kiểm tra nguyên tệ detail có bằng dòng gốc không
            if (valueDetail.length > 0) {
                var dt_amount_nguyente = 0;
                var dt_amount_quydoi = 0;
                var dt_amount_thuent = 0;
                var dt_amount_thueqd = 0;
                valueDetail.forEach((element) => {
                    if (isNaN(element[dt_nguyente]) || element[dt_nguyente] !== " ") {
                        dt_amount_nguyente += parseFloat(element[dt_nguyente].replace(',', ''))
                    }
                    if (isNaN(element[dt_quydoi]) || element[dt_quydoi] !== " ") {
                        dt_amount_quydoi += parseFloat(element[dt_quydoi].replace(',', ''))
                    }
                    if (isNaN(element[dt_thuent]) || element[dt_thuent] !== " ") {
                        dt_amount_thuent += parseFloat(element[dt_thuent].replace(',', ''))
                    }
                    if (isNaN(element[dt_thueqd]) || element[dt_thueqd] !== " ") {
                        dt_amount_thueqd += parseFloat(element[dt_thueqd].replace(',', ''))
                    }
                })
                if (parseFloat(master_model._data[i][ms_nguyente].replace(',', '')) != dt_amount_nguyente) {
                    notification += `Tổng nguyên tệ dòng tách của doanh thu ${master_model._data[i][doanhThuId]}  không bằng dòng gốc \n`;
                }
                if (parseFloat(master_model._data[i][ms_quydoi].replace(',', '')) != dt_amount_quydoi) {
                    notification += `Tổng quỷ đổi dòng tách của doanh thu ${master_model._data[i][doanhThuId]} không bằng dòng gốc \n`;
                }
                if (parseFloat(master_model._data[i][ms_thuent].replace(',', '')) != dt_amount_thuent) {
                    notification += `Tổng thuế nt dòng tách của doanh thu ${master_model._data[i][doanhThuId]} không bằng dòng gốc \n`;
                }
                if (parseFloat(master_model._data[i][ms_thueqd].replace(',', '')) != dt_amount_thueqd) {
                    notification += `Tổng thuế qđ dòng tách của doanh thu ${master_model._data[i][doanhThuId]} không bằng dòng gốc \n`;
                }
            }
        }

        if (notification.length > 0) {
            apex.message.alert(notification);
            return;
        }
        if (notification.length == 0) {
            // check dupplication
            detail_model.forEach((element, index) => {
                if (element[0] != 'Y' && element[flagDeleteDetail] == 0) {
                    var loaiThueGTGT = detail_model.getFieldKey("VAT_OBJECT_TYPE_ID");
                    var doiTuongThueGTGT = detail_model.getFieldKey("VAT_OBJECT_ID");
                    var thueRaVao = detail_model.getFieldKey("VAT_IN_OUT");
                    var loaiThue = detail_model.getFieldKey("VAT_TYPE");
                    var loaiDTNo = detail_model.getFieldKey("DEBIT_OBJECT_TYPE_ID");
                    var doiTuongNo = detail_model.getFieldKey("DEBIT_OBJECT_ID");
                    var loaiDTCo = detail_model.getFieldKey("CREDIT_OBJECT_TYPE_ID");
                    var doiTuongCo = detail_model.getFieldKey("CREDIT_OBJECT_ID");
                    var nhomThue = detail_model.getFieldKey("VAT_GROUP_ID");
                    var loaiDTDongThue = detail_model.getFieldKey("TAX_OBJECT_TYPE_ID");
                    var doiTuongDongThue = detail_model.getFieldKey("TAX_OBJECT_ID");
                    var KM1 = detail_model.getFieldKey("ANA01_ID");
                    var KM2 = detail_model.getFieldKey("ANA02_ID");
                    var KM3 = detail_model.getFieldKey("ANA03_ID");
                    console.log('elementelement', element)
                    createListCompareDuplicate(listDupLoaiThueGTGT, listDupDTThueGTGT, listDupThueRaVao, listDupLoaiThue, listDupLoaiDTNo,
                        listDupDTNo, listDupLoaiDTCo, listDupDTCo, listDupNhomThue, listDupLoaiDTDongThue, listDupDTDongThue,
                        listDupKM1, listDupKM2, listDupKM3, element[loaiThueGTGT], element[doiTuongThueGTGT], element[thueRaVao], element[loaiThue], element[loaiDTNo], element[doiTuongNo]
                        , element[loaiDTCo], element[doiTuongCo], element[nhomThue], element[loaiDTDongThue], element[doiTuongDongThue], element[KM1], element[KM2], element[KM3])
                }
            })

            messageListError = checkDataDuplicate(listDupLoaiThueGTGT, listDupDTThueGTGT, listDupThueRaVao, listDupLoaiThue, listDupLoaiDTNo,
                listDupDTNo, listDupLoaiDTCo, listDupDTCo, listDupNhomThue, listDupLoaiDTDongThue, listDupDTDongThue,
                listDupKM1, listDupKM2, listDupKM3);

            if (messageListError != '') {
                apex.message.alert(messageListError);
                return;
            }
        }

        if (notification.length == 0 && messageListError == '') {
            console.log('anhoi', detail_model);
            detail_model._data.filter(x => x[flagDeleteDetail] == 1).forEach(e => {
                var idTemp = detail_model.getFieldKey("ID");
                var isDeletedTemp = detail_model.getFieldKey("IS_DELETED");
                detail_model.setValue(e, 'IS_DELETED', 'Y')
            })
            detail_model.save();
            apex.message.showPageSuccess("Lưu dữ liệu thành công");
            return;
        }
    } catch (error) {
        console.log('error catch', error)
    }
}

const createListCompareDuplicate = (listDupLoaiThueGTGT, listDupDTThueGTGT, listDupThueRaVao, listDupLoaiThue, listDupLoaiDTNo,
    listDupDTNo, listDupLoaiDTCo, listDupDTCo, listDupNhomThue, listDupLoaiDTDongThue, listDupDTDongThue,
    listDupKM1, listDupKM2, listDupKM3, valueLoaiThueGTGT, valueDoiTuongThueGTGT, valueThueRaVao, valueLoaiThue, valueLoaiDTNo, valueDoiTuongNo
    , valueLoaiDTCo, valueDoiTuongCo, valueNhomThue, valueLoaiDTDongThue, valueDoiTuongDongThue, valueKM1, valueKM2, valueKM3) => {

    listDupLoaiThueGTGT.push(valueLoaiThueGTGT);
    listDupDTThueGTGT.push(valueDoiTuongThueGTGT);
    listDupThueRaVao.push(valueThueRaVao);
    listDupLoaiThue.push(valueLoaiThue);
    listDupLoaiDTNo.push(valueLoaiDTNo);
    listDupDTNo.push(valueDoiTuongNo);
    listDupLoaiDTCo.push(valueLoaiDTCo);
    listDupDTCo.push(valueDoiTuongCo);
    listDupNhomThue.push(valueNhomThue);
    listDupLoaiDTDongThue.push(valueLoaiDTDongThue);
    listDupDTDongThue.push(valueDoiTuongDongThue);
    listDupKM1.push(valueKM1);
    listDupKM2.push(valueKM2);
    listDupKM3.push(valueKM3);
}

const checkDataDuplicate = (listDupLoaiThueGTGT, listDupDTThueGTGT, listDupThueRaVao, listDupLoaiThue, listDupLoaiDTNo,
    listDupDTNo, listDupLoaiDTCo, listDupDTCo, listDupNhomThue, listDupLoaiDTDongThue, listDupDTDongThue,
    listDupKM1, listDupKM2, listDupKM3) => {
    var checkListDupLoaiThueGTGT = new Set(listDupLoaiThueGTGT)
    var checkListDupDTThueGTGT = new Set(listDupDTThueGTGT)
    var checkListDupThueRaVao = new Set(listDupThueRaVao)
    var checklistDupLoaiThue = new Set(listDupLoaiThue)
    //var checklistDupLoaiHoaDon = new Set(listDupLoaiHoaDon)
    var checklistDupLoaiDTNo = new Set(listDupLoaiDTNo)
    var checklistDupDTNo = new Set(listDupDTNo)
    var checklistDupLoaiDTCo = new Set(listDupLoaiDTCo)
    var checklistDupDTCo = new Set(listDupDTCo)
    var checklistDupNhomThue = new Set(listDupNhomThue)
    var checklistDupLoaiDTDongThue = new Set(listDupLoaiDTDongThue)
    var checklistDupDTDongThue = new Set(listDupDTDongThue)
    var checklistDupKM1 = new Set(listDupKM1)
    var checklistDupKM2 = new Set(listDupKM2)
    var checklistDupKM3 = new Set(listDupKM3)
    console.log('co chay vao day ko', listDupLoaiThueGTGT)
    messageError = '';
    if (checkListDupLoaiThueGTGT.size > 1) {

        messageError += 'checkListDupLoaiThueGTGT chưa trùng khớp \n';
    }
    if (checkListDupDTThueGTGT.size > 1) {
        messageError += 'checkListDupDTThueGTGT chưa trùng khớp \n';
    }
    if (checkListDupThueRaVao.size > 1) {
        messageError += 'checkListDupThueRaVao chưa trùng khớp \n';
    }
    if (checklistDupLoaiThue.size > 1) {
        messageError += 'checklistDupLoaiThue chưa trùng khớp \n';
    }
    if (checklistDupLoaiDTNo.size > 1) {
        messageError += 'checklistDupLoaiDTNo chưa trùng khớp \n';
    }
    if (checklistDupDTNo.size > 1) {
        messageError += 'checklistDupDTNo chưa trùng khớp \n';
    }
    if (checklistDupLoaiDTCo.size > 1) {
        messageError += 'checklistDupLoaiDTCo chưa trùng khớp \n';
    }
    if (checklistDupDTCo.size > 1) {
        messageError += 'checklistDupDTCo chưa trùng khớp \n';
    }
    if (checklistDupNhomThue.size > 1) {
        messageError += 'checklistDupNhomThue chưa trùng khớp \n';
    }
    if (checklistDupLoaiDTDongThue.size > 1) {
        messageError += 'checklistDupLoaiDTDongThue chưa trùng khớp \n';
    }
    if (checklistDupDTDongThue.size > 1) {
        messageError += 'checklistDupDTDongThue chưa trùng khớp \n';
    }
    if (checklistDupKM1.size > 1) {
        messageError += 'checklistDupKM1 chưa trùng khớp \n';
    }
    if (checklistDupKM2.size > 1) {
        messageError += 'checklistDupKM2 chưa trùng khớp \n';
    }
    if (checklistDupKM3.size > 1) {
        messageError += 'checklistDupKM3 chưa trùng khớp \n';
    }
    return messageError;
}

const themDong = (del_row) => {
    console.log('del_row value', del_row);
    // lấy giá trị các cột tương ứng trên bảng master
    var model_master = apex.region("master").widget().interactiveGrid("getViews", "grid").model;
    var detail_model = apex.region("detail").widget().interactiveGrid("getViews", "grid").model;
    console.log('detail_model', detail_model);

    var table_selectedRecords = model_master.getSelectedRecords();
    var master_series = model_master.getValue(table_selectedRecords[0], "SERIAL");
    var master_hoadon = model_master.getValue(table_selectedRecords[0], "REF_NO");
    var master_ana03_id = model_master.getValue(table_selectedRecords[0], "ANA03_ID");
    var master_ngayhd = model_master.getValue(table_selectedRecords[0], "REF_DATE");
    var master_loaitien = model_master.getValue(table_selectedRecords[0], "CURRENCY_ID");
    var master_tygia = model_master.getValue(table_selectedRecords[0], "EXCHANGE_RATE");
    var master_mst = model_master.getValue(table_selectedRecords[0], "VAT_NO");
    var master_diengiaict = model_master.getValue(table_selectedRecords[0], "REF_DESC");
    var master_loaiphieu = model_master.getValue(table_selectedRecords[0], "VOUCHER_TYPE_ID");
    var master_sophieu = model_master.getValue(table_selectedRecords[0], "VOUCHER_NO");
    var master_ngayphieu = model_master.getValue(table_selectedRecords[0], "VOUCHER_DATE");
    var master_diengiaiphieu = model_master.getValue(table_selectedRecords[0], "VOUCHER_DESC");
    var master_loaithuegtgt = model_master.getValue(table_selectedRecords[0], "VAT_OBJECT_TYPE_ID");
    var master_dtgtgt = model_master.getValue(table_selectedRecords[0], "VAT_OBJECT_ID");
    var master_loaidtno = model_master.getValue(table_selectedRecords[0], "DEBIT_OBJECT_TYPE_ID");
    var master_dt_no = model_master.getValue(table_selectedRecords[0], "DEBIT_OBJECT_ID");
    var master_loaidtco = model_master.getValue(table_selectedRecords[0], "CREDIT_OBJECT_TYPE_ID");
    var master_dt_co = model_master.getValue(table_selectedRecords[0], "CREDIT_OBJECT_ID");
    var master_loaidtthue = model_master.getValue(table_selectedRecords[0], "TAX_OBJECT_TYPE_ID");
    var master_dt_thue = model_master.getValue(table_selectedRecords[0], "TAX_OBJECT_ID");
    var master_doanhthu_id = model_master.getValue(table_selectedRecords[0], "ID");
    console.log('master_loaiphieu', master_loaiphieu);
    // tham chiếu, gán các giá trị từ dòng master xuống dòng detail
    var model_detail = apex.region("detail").widget().interactiveGrid("getViews", "grid").model;
    var table_detail = model_detail.getSelectedRecords();
    var myNewRecordId = model_detail.insertNewRecord();
    var myNewRecord = model_detail.getRecord(myNewRecordId);

    model_detail.setValue(myNewRecord, 'SERIAL', master_series);
    model_detail.setValue(myNewRecord, 'REF_NO', master_hoadon);
    model_detail.setValue(myNewRecord, 'ANA03_ID', master_ana03_id);
    model_detail.setValue(myNewRecord, 'REF_DATE', master_ngayhd);
    model_detail.setValue(myNewRecord, 'CURRENCY_ID', master_loaitien);
    model_detail.setValue(myNewRecord, 'EXCHANGE_RATE', master_tygia);

    model_detail.setValue(myNewRecord, 'VAT_NO', master_mst);
    model_detail.setValue(myNewRecord, 'REF_DESC', master_diengiaict);
    
    model_detail.setValue(myNewRecord, 'VOUCHER_TYPE_ID', master_loaiphieu);
    model_detail.setValue(myNewRecord, 'VOUCHER_NO', master_sophieu);
    model_detail.setValue(myNewRecord, 'VOUCHER_DATE', master_ngayphieu);
    model_detail.setValue(myNewRecord, 'VOUCHER_DESC', master_diengiaiphieu);
    model_detail.setValue(myNewRecord, 'VAT_OBJECT_TYPE_ID', master_loaithuegtgt);
    model_detail.setValue(myNewRecord, 'VAT_OBJECT_ID', master_dtgtgt);
    model_detail.setValue(myNewRecord, 'DEBIT_OBJECT_TYPE_ID', master_loaidtno);
    model_detail.setValue(myNewRecord, 'DEBIT_OBJECT_ID', master_dt_no);
    model_detail.setValue(myNewRecord, 'CREDIT_OBJECT_TYPE_ID', master_loaidtco);
    model_detail.setValue(myNewRecord, 'CREDIT_OBJECT_ID', master_dt_co);
    model_detail.setValue(myNewRecord, 'TAX_OBJECT_TYPE_ID', master_loaidtthue);
    model_detail.setValue(myNewRecord, 'TAX_OBJECT_ID', master_dt_thue);
    model_detail.setValue(myNewRecord, 'O_AMOUNT', "0");
    model_detail.setValue(myNewRecord, 'C_AMOUNT', "0");
    model_detail.setValue(myNewRecord, 'TAX_O_AMOUNT', "0");
    model_detail.setValue(myNewRecord, 'TAX_C_AMOUNT', "0");
    model_detail.setValue(myNewRecord, 'Delete_Row', )

    var id = model_detail.setValue(myNewRecord, 'DOANH_THU_TOPO_ID', master_doanhthu_id);
}

const getValueDelete = (ID) => {
    var detail_model = apex.region("detail").widget().interactiveGrid("getViews", "grid").model;
    var delete_row = detail_model.getFieldKey("Delete_Row");
    var flagDelete = detail_model.getFieldKey("FlagDelete");
    var idDetail = detail_model.getFieldKey("ID");
    var voucherTypeId = detail_model.getFieldKey("VOUCHER_TYPE_ID");
    console.log('VOUCHER_TYPE_ID anhvt2', voucherTypeId);
    // hành động khi click vào span xóa dòng
    // lấy Id của dòng đó
    console.log('getValueDelete anhvt2', ID);
    var view = apex.region("detail").call("getCurrentView");
    console.log('view', view);
    
    // console.log("selection changed here is the active row ", view.getContextRecord(document.activeElement));
    // var rowActiveElement = view.getContextRecord(document.activeElement);
    // console.log('rowActiveElement', rowActiveElement);
    var viewDetail = apex.region("detail").widget().interactiveGrid("getViews").grid.view$;
    console.log('viewMaster', viewDetail);
    // viewMaster.grid("option").columns[0].VOUCHER_TYPE_ID.cellTemplate ="<s>anhvt22</s>";
    // viewMaster.grid("refresh");

    // $("#a-GV-row ")
    // xử lý trên html check IG detail thông qua data-id
    // lấy html của interactive grid detail
    var viewHTMLGridDetail = document.getElementById('detail_ig_grid_vc');
    var viewHTMLGridBodyDetail = viewHTMLGridDetail.getElementsByClassName("a-GV-w-scroll").item("a-GV-row").getElementsByTagName("tbody")[0].childNodes;
    console.log('viewHTMLGridBodyDetail', viewHTMLGridBodyDetail)
    viewHTMLGridBodyDetail.forEach(elementHTML => {
        if(elementHTML.matches(':hover')) {
            // update value in model_detail flagDelete
            detail_model._data.filter(x => x[idDetail] == elementHTML.dataset.id).forEach(e => {
                if(e[flagDelete] == 0) {
                    elementHTML.style.textDecoration = 'line-through';
                    detail_model.setValue(e, 'FlagDelete', 1);
                } else {
                    elementHTML.style.textDecoration = 'none';
                    detail_model.setValue(e, 'FlagDelete', 0);
                }
            })
        }
    })
}